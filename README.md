#iTracks

iTracks is aan Android Project made as part of the coding challenge for Android Developer position
at Appetiser.

##Libraries

1. Retrofit - used to parse JSON response from a service as provided in the code challenge

2. Gson Converter - used in conjucntion with Retrofit to process JSON response and convert to proper 
            Java's POJO
            
3. GSON - used to process a list of Track objects and convert it to JSON string to be stored and used for 
            data persistence

4. Picasso - used to efficiently render images fetched from the api and easily render placeholder images
            when the image from the server in unavailable
            
5. Cardview - used for item layout in recyclerview

6. Recyclerview - used to iterate and display list of track items fetched from the api