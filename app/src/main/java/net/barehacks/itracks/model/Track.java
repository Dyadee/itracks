package net.barehacks.itracks.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @Class
 * This class represents a model that converts a JSON element
 * into a JAVA object POJO.
 * The class implements Parcelable interface in order to
 * have capacity to bundle an instance and send to another activity.
 *
 *
 * */
public class Track implements Parcelable {
    @SerializedName("trackName")
    @Expose
    private String title;
    @SerializedName("artworkUrl100")
    @Expose
    private String thumbnail;
    @SerializedName("trackPrice")
    @Expose
    private String price;
    @SerializedName("primaryGenreName")
    @Expose
    private String genre;
    @SerializedName("longDescription")
    @Expose
    private String description;

    protected Track(Parcel in) {
        title = in.readString();
        thumbnail = in.readString();
        price = in.readString();
        genre = in.readString();
        description = in.readString();
    }

    public static final Creator<Track> CREATOR = new Creator<Track>() {
        @Override
        public Track createFromParcel(Parcel in) {
            return new Track(in);
        }

        @Override
        public Track[] newArray(int size) {
            return new Track[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getPrice() {
        return price;
    }

    public String getGenre() {
        return genre;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(thumbnail);
        dest.writeString(price);
        dest.writeString(genre);
        dest.writeString(description);
    }
}
