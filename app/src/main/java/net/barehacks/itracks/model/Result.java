package net.barehacks.itracks.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/***
 * @Class
 * This class separates the resulting JSON from a single element [result count]
 *  which is an integer to an array of elements that contains tracks.
 * */

public class Result {

    @SerializedName("resultCount")
    private int resultCount;
    @SerializedName("results")
    private List<Track> tracks;


    public int getResultCount() {
        return resultCount;
    }

    public List<Track> getTracks() {
        return tracks;
    }
}
