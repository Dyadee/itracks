package net.barehacks.itracks;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import net.barehacks.itracks.model.Track;
/***
 * @Class
 * This class handles the details of the tracks, loaded when an item from the recyclerview is clicked.
 * This class includes to display the long description of the track
 * */
public class TrackDetailActivity extends AppCompatActivity {
    TextView trackTitle, trackGenre, trackPrice, trackDescription;
    ImageView trackArtwork;
    private static final String TAG = "TrackDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_detail);

        trackTitle = findViewById(R.id.track_detail_title);
        trackGenre = findViewById(R.id.track_detail_genre);
        trackPrice = findViewById(R.id.track_detail_price);
        trackDescription = findViewById(R.id.track_detail_description);
        trackArtwork = findViewById(R.id.track_detail_artwork);

        Track trackDetail = getIntent().getParcelableExtra("TrackDetail");

        String artwork100 = trackDetail.getThumbnail();
        String newArtwork200 = artwork100.replace("100x100", "300x300");
        Log.d(TAG, "onCreate: "+ newArtwork200);

        Picasso.get().load(newArtwork200).placeholder(R.drawable.image_placeholder).into(trackArtwork);

        trackTitle.setText(trackDetail.getTitle());
        trackGenre.setText("| "+trackDetail.getGenre());
        trackPrice.setText("$"+trackDetail.getPrice());
        trackDescription.setText(trackDetail.getDescription());

    }


}
