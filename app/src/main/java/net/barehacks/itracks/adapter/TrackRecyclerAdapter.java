package net.barehacks.itracks.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import net.barehacks.itracks.R;
import net.barehacks.itracks.TrackDetailActivity;
import net.barehacks.itracks.model.Track;

import java.util.List;

/***
 * @Class
 * This class serves as the data controller for the recyclerview taht displays the master list
 * of tracks as result of the search api.
 * */

public class TrackRecyclerAdapter extends RecyclerView.Adapter<TrackRecyclerAdapter.MyViewHolder> {

    private List<Track> mtracks;
    private LayoutInflater mlayoutInflater;
    private Context mContext;
    private static final String TAG = "TrackRecyclerAdapter";

    public TrackRecyclerAdapter(Context context, List<Track> tracks) {
        this.mtracks = tracks;
        this.mlayoutInflater = LayoutInflater.from(context);
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mlayoutInflater.inflate(R.layout.track_item,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Track currentTrack = mtracks.get(position);
        holder.loadData(currentTrack, position);

    }

    @Override
    public int getItemCount() {
        return mtracks.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView track_title, track_price, track_genre;
        ImageView track_image;
        int position;
        Track currentTrack;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            track_title = itemView.findViewById(R.id.track_title);
            track_genre = itemView.findViewById(R.id.track_genre);
            track_price = itemView.findViewById(R.id.track_price);
            track_image = itemView.findViewById(R.id.artwork);
            itemView.setOnClickListener(MyViewHolder.this);
        }
        /***
         * @Method
         * This method loads each single data from a list of tracks into a viewholder
         * in each item in recyclerview
         * */
        public void loadData(Track currentTrack, int position) {

            this.track_title.setText(currentTrack.getTitle());
            this.track_genre.setText(currentTrack.getGenre());
            this.track_price.setText("Price: $"+ currentTrack.getPrice());
            Picasso.get().load(currentTrack.getThumbnail()).placeholder(R.drawable.image_placeholder).into(track_image);

            this.position = position;
            this.currentTrack = currentTrack;


        }
        /***
         * @Method
         * This method is triggered when an item in the recyclerview is clicked
         * The currentTrack that is passed as an Extra argument is a bundle made possible by
         * the Parcelable interface
         * */
        @Override
        public void onClick(View v) {
            Intent trackDetailActivity = new Intent(mContext, TrackDetailActivity.class);
            trackDetailActivity.putExtra("TrackDetail", currentTrack);
            mContext.startActivity(trackDetailActivity);

        }
    }
}
