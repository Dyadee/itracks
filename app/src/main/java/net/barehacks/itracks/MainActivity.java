package net.barehacks.itracks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import net.barehacks.itracks.adapter.TrackRecyclerAdapter;
import net.barehacks.itracks.api.TrackAPI;
import net.barehacks.itracks.model.Result;
import net.barehacks.itracks.model.Track;
import net.barehacks.itracks.utils.JsonStorage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/***
 * @Class
 * This class serves as the class that processes the Main Screen of the app
 * This is where the data fethced from the search api is displayed in a recyclerview
 * */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    /***
     * */
    List<Track> trackList = new ArrayList<>();
    Gson gson;
    JsonElement element;
    JsonArray jsonArray;
    JsonStorage mfile = new JsonStorage();
    private TextView show_date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        show_date = findViewById(R.id.showdate);
        gson = new Gson();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TrackAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TrackAPI trackAPI = retrofit.create(TrackAPI.class);

        Call<Result> call = trackAPI.getResults();

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if (!response.isSuccessful()) {
                    show_date.setText("Not Ok!");
                } else {
                    trackList = response.body().getTracks();
                    loadRecyclerView(trackList);

                    element = gson.toJsonTree(trackList, new TypeToken<List<Track>>() {
                    }.getType());
                    jsonArray = element.getAsJsonArray();
                    mfile.writeToFile(jsonArray.toString(), MainActivity.this);

                    SharedPreferences sharedPreferences = getSharedPreferences(getPackageName() + "datevisited", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
                    String datetime = simpleDateFormat.format(new Date());

                    editor.putString("showdate", datetime);
                    editor.apply();
                    show_date.setText("Last Visited: " + datetime);
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                try {
                    String jsonTracks = mfile.readFromFile(MainActivity.this);
                    if (jsonTracks != "") {

                        trackList = gson.fromJson(jsonTracks,
                                new TypeToken<ArrayList<Track>>() {
                                }.getType());
                        loadRecyclerView(trackList);
                        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName() + "datevisited", Context.MODE_PRIVATE);
                        String showdate = sharedPreferences.getString("showdate", "error");
                        show_date.setText("Last Visited: " + showdate);
                    }


                } catch (Exception e) {
                    show_date.setText("Server cannot be reached");
                    e.printStackTrace();
                }
            }
        });

    }

    private void loadRecyclerView(List<Track> tracks) {
        RecyclerView recyclerView = findViewById(R.id.tracks_recyclerview);
        TrackRecyclerAdapter trackRecyclerAdapter = new TrackRecyclerAdapter(MainActivity.this, tracks);
        recyclerView.setAdapter(trackRecyclerAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

}
