package net.barehacks.itracks.api;

import net.barehacks.itracks.model.Result;

import retrofit2.Call;
import retrofit2.http.GET;


/***
 * @Interface
 * This interface is a required interface in order for Retrofit library to work
 *
 * */
public interface TrackAPI {
    String BASE_URL = "https://itunes.apple.com/";

    @GET("search?term=star&country=au&media=movie&all")
    Call<Result> getResults();

}
